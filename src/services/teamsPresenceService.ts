import fetch from 'node-fetch';
import { Logger } from '../models';

export class TeamsPresenceService {
  
  /**
   * Perform a get request against the MS Graph
   * 
   * @param url 
   * @param accessToken 
   */
  public static async get(url: string, accessToken: string, identifier: string, log: Logger, debug: boolean = false) {
    if (debug) {
      log.info(`Calling the MS Graph.`);
    }

    const response = await fetch(url, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
        'Content-Type': `application/json`,
        'Accept': `application/json`
      },
      method: 'post',
      body: JSON.stringify(identifier)
    });

    const data = await response.json();
    if (debug) {
      log.info(`Get presence response: ${JSON.stringify(data)}`);
    }
    
    return data;
  }
}